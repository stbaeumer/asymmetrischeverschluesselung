# Die asymmetrische Verschlüsselung #

Bei der Verschlüsselung von Text, Dateien oder ganzen Ordnern werden das symmetrische und das asymmetrische Verfahren unterschieden.

Das symmetrische Verfahren ist in seinem Ablauf für jedermann leicht nachvollziehbar: Beim Verschlüsseln wird ein Kennwort eingetippt, das beim Entschlüsseln erneut eingegeben werden muss. Die Abbildung zeigt, wie eine Datei mit dem Programm *7zip* mittels Rechtsklick mit einem Passwort symmetrisch verschlüsselt wird. 

![7-zip](7zip.PNG)

Nachteilig ist das symmetrische Verfahren, sobald die Datei verschlüsselt über das Internet an einen Empfänger gesendet werden soll. Das Problem liegt darin, dass das Passwort zuvor mit dem Empfänger verabredet werden muss. Das wiederum müsste persönlich, telefonisch oder postalisch geschehen und kann keinesfalls unverschlüsselt über das Internet erfolgen. Für eine regelmäßige Kommunikation mit unterschiedlichsten Empfängern ist das Verfahren ungeeignet.

Dieses Problem löst die asymmertrische Verschlüsselung: Wer verschlüsselte Dateien / Botschaften sicher über das Internet empfangen möchte, muss sich einmalig ein Schlüsselpaar generieren. So ein Schlüsselpaar besteht immer aus einem *privaten Schlüssel* und einem *öffentlichen Schlüssel*. Beide Schlüssel sind eine (ziemlich lange) Folge von Zahlen und Buchstaben, die sich jeweils als Textdatei (mit der Dateiendung .asc) speichern lassen.  

Sie können die beiden Schlüsseldateien problemlos im Editor (z. B. Notepad) öffnen und anschauen. Der Inhalt der Public-Key-Datei sieht dann (gekürzt) in etwa so aus:

```
-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: GnuPG v1

mQENBFZxgLgBCACbg5OKaoe7twtWTrfDGvVMZZSvg2GKe9qrKDcLu1eb55+k8cpH
...
=8B17
-----END PGP PUBLIC KEY BLOCK-----

```

Das, was Sie in der Datei sehen ist nicht etwa ein extralanges Kennwort, das Sie womöglich irgendwo eintippen müssen. Stattdessen muss so eine Schlüsseldatei in einem Entschlüsselungsprogramm eingebunden werden, um dann ver-/entschlüsseln zu können.

Der Unterschied zwischen beiden Schlüsseln ist:

* Der öffentliche Schlüssel kann nur verschlüsseln.
* Der private Schlüssel zusätzlich auch entschlüsseln. 

Also kann jedermann beliebige Dateien mit irgendwelchen öffentlichen Schlüsseln verschlüsseln. Das Entschlüsseln ist dann aber ausschließlich demjenigen möglich, der den passenden privaten Schlüssel hat. Daraus ergibt sich, dass der private Schlüssel sicher verwahrt werden muss, während der öffentliche Schlüssel ganz bewusst demjenigen zur Verfügung gestellt wird, der etwas Verschlüsseltes zusenden soll. 

Um ganz sicher zu gehen, dass der private Schlüssel nicht in falsche Hände gerät, ist er zusätzlich symmetrisch mit einer sogenannten Passphrase verschlüsselt. Immer, wenn der private Schlüssel zum Einsatz kommen soll, muss er zuvor mit der Passphrase entschlüsselt werden. Eine Passphrase ist im Grunde so etwas wie ein Kennwort. Allerdings erlauben Passphrasen auch Leerzeichen, wodurch sich auch längere Sätze bilden lassen. Eine Passphrase könnte also sein: *Hänschen kl1 g!ng all1.* 

## Wie wird's gemacht?

Windows-Freunde installieren sich das Programm ```gpg4win.exe```. Darin enthalten ist ein Programm namens Kleopatra, das im Folgenden besprochen werden soll. Zudem wird auch noch ein Plugin in Outlook installiert, mit dem die Ver-/Entschlüsselung in Outlook möglich wird.

### Noch kein Schlüssel erstellt ...

Falls nach der Installation noch kein Schlüsselpaar jemals erstellt wurde, kann das mit Kleopatra gemacht werden. Klicken Sie auf "Schlüsselpaar erstellen". Sie werden aufgefordert Ihren Namen und Ihre Adresse einzugeben. Was Sie dort eingeben ist grundsätzlich egal. Allerdings sollten Sie tatsächlich Ihren echten Namen und die echte Webweaver-Adresse verwenden. Anschließend müssen Sie die oben erwähnte Passphrase festlegen.

![Neu](neu.PNG)

Nach dem erfolgreichen Anlegen sollte ein Backup des Schlüsselpaares vorgenommen werden. Das Schlüsselpaar-Backup muss sicher verwahrt werden, z. B. auf einem USB-Stick im Bankschließfach.

### Falls bereits ein Schlüsselpaar existiert ...

Nach erneuten Start von Kleopatra wird der vorhandene eigene Schlüssel angezeigt. Das gilt leider nicht für die Schulumgebung. Dort muss nach einem Neustart ein Import aus dem Backup vorgenommen werden. Auf die gleiche Weise müssen öffentliche Schlüssel derer importiert werden, denen etwas geschickt werden soll. Sie können die Schlüssel auch einfach mit der Maus auf Kleopatra ziehen und fallenlassen.

## Dateien verschlüsseln

Voraussetzung ist, dass ein öffentlicher Schüssel in Kleopatra existiert. Klicken Sie im Windows-Explorer mit Rechtsklick auf eine zu verschlüsselnde Datei. Wählen Sie *verschlüsseln*. Zuletzt müssen Sie wählen, ob Sie die Datei nur für sich selbst verschlüsseln wollen oder für einen anderen Empfänger. Wählen Sie den Empfänger aus der Liste der in Ihrem Kleopatra importieren öffentlichen Schlüssel.
Wenn das geschehen ist, sehen Sie im Explorer zwei Dateien. Neben der Originaldatei liegt nun die verschlüsselte Datei. Sie können Sie verschlüsselte Datei nun als Mail-Anhang senden.

## Dateien entschlüsseln

Voraussetzung für das Entschlüsseln mit Ihrem privaten Schlüssel ist, dass die Datei (von wem auch immer) mit Ihrem öffentlichen Schlüssel verschlüsselt wurde. Wenn das zutrifft, dann rechtsklicken Sie die Datei. Wählen Sie *entschlüsseln* und geben Sie die Passphrase ein. Nun sollte die entschlüsselte Datei im Explorer vor Ihnen liegen.

## Wie kann mir der Lehrer verschlüsselte Nachrichten senden?

Exportieren Sie Ihren öffentlichen Schlüssel aus Kleopatra. Sie erhalten sodann eine Textdatei, die Sie irgendwohin speichern können. Öffnen Sie die Textdatei mit einem Editor (z. B. Notepad), um sicherzugehen, dass es sich bei dem Schlüssel um den öffentlichen und nicht den privaten handelt! Die erste Zeile zeigt Ihnen das an. Senden Sie Ihren öffentlichen Schlüssel an stefan.baeumer@berufskolleg-borken.de. *WICHTIG:* Wenn Sie die Datei senden, stimmen Sie damit automatisch zu, dass Ihnen der Lehrer personenbezogene Daten verschlüsselt per E-Mail zusenden darf. 

## Hinter den Kulissen

Beachten Sie, dass Kleopatra nur die Benutzeroberfläche ist, die sich benutzerfreundlich um eine Technologie legt, die eigentlich GnuPG heißt. GnuPG ist heute der Standard, wenn es um asymmetriesche Verschlüsselung im Internet geht.

### Ist GnuPG sicher?

Ja. Das mathematische Verfahren hinter GnuPG gilt als sicher. Sollten Sie einmal über ein Sicherheitsproblem rund um Verschlüsselung lesen, dann ist nicht die Sicherheit von GnuPG gemeint, sondern beispielsweise die Einbindung von GnuPG in ein Programm wie Kleopatra.